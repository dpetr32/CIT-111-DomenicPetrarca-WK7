package Employees;

public class EmployeeLocation {
	
	public static void main(String[] args) {
		
		Employee clerk = new Employee();
		Employee driver = new Employee();
		Employee manager = new Employee();
		Employee clerk2 = new Employee();
		
		
		//Set a Value
		clerk.setEmployeeLocation("Ontario");
		driver.setEmployeeLocation("ChinaTown, NY");
		manager.setEmployeeLocation("Beverly Hill, CA");
		clerk2.setEmployeeLocation("Youngstown, PA");
		
		
		//Get Information
		System.out.println("The Clerk's Location is " + clerk.getEmployeeLocation());
		
		System.out.println("");
		
		System.out.println("The Driver's Location is " + driver.getEmployeeLocation());
		
		System.out.println("");
		
		System.out.println("The Managers Location is " + manager.getEmployeeLocation());
		
		System.out.println("");
		
		System.out.println("The Second Clerk's Location is " + clerk2.getEmployeeLocation());
		
	
	
	}
}
