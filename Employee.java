package Employees;

public class Employee {
	//Declare Instance Variables
		private int EmployeeNumber;
		private String EmployeeLastName;
		private String EmployeeFirstName;
		private String EmployeeLocation;
		private double EmployeeSalary;
		
		//Getter
		public int getEmployeeNumber()
		{
			return EmployeeNumber;
		}
		
		//Setter
		public void setEmployeeNumber(int empno) 
		{
			EmployeeNumber = empno; //This is the value that user sends us
		}
		
		
		//Getter
		public String getEmployeeLastName() 
		{
			return EmployeeLastName;
		}
		
		//Setter
		public void setEmployeeLastName(String fname)
		{
			EmployeeLastName = fname;
		}
		
		//Getter
		public String getEmployeeFirstName() 
		{
			return EmployeeFirstName;
		}
		
		//Setter
		public void setEmployeeFirstName(String fname) 
		{
		EmployeeFirstName = fname;
		}
		
		public double getEmployeeSalary()
		{
			return EmployeeSalary;
		}
		
		public void setEmployeeSalary (double salary)
		{
			EmployeeSalary = salary;
		}
		
		
		//Getter
		public String getEmployeeLocation() 
				{
					return EmployeeLocation;
				}
				
				//Setter
		public void setEmployeeLocation(String Location)
				{
					EmployeeLocation = Location;
				}
		
}
